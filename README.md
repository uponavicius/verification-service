# Verification-service
## **Introduction**
Welcome to the **Email verification and password reset service**
With this app can verify email and reset password
**ATTENTION!** Verify email can without this service. With this service just more beautiful verification text in a browser.

### First run FamFin-app-ws 
FamFin-app-ws project https://gitlab.com/uponavicius/famfin-app-ws

### Run Verification service
1. Download verification-service-master.zip.
2. Unpack to your desired location.
3. Open command prompt `Windows+R`, write cmd and press enter, the go to verification-service-master root catalog.
    - Example `cd C:\Users\user_name\Downloads\verification-service-master`.
4. Then run command `mvn install` and close command prompt. This command generate `verification-service.war` file.
5. Open Apache Tomcat and deploy `verification-service.war` from example `C:\Users\user_name\Downloads\verification-service-master\target`.
   - Install and run Apache Tomcat https://www.youtube.com/watch?v=K4ZPCwtmV1Q
   - After installation run Apache Tomcat service
   - Open on a browser this link http://localhost:8080/manager/html ant type your created credentials.
   - Deploy verification-service.war https://www.youtube.com/watch?v=zehzqr3QYfw
   - Open on a browser this links http://localhost:8080/verification-service/ On browser you will to see **FamFinApp verification service**
   
### FamFin application verification service
Family Finances application verification service https://gitlab.com/uponavicius/verification-service



##### Deploy FamFin-app-ws and verification-service on Apache Tomcat
FamFin-app-ws project - https://gitlab.com/uponavicius/famfin-app-ws


##### Email verification
- Deploy FamFin-app-ws and verification-service on Apache Tomcat
- Create new user with Postman

| Action      | Method | URL                                 | Body JSON Example                                                                                       | Header KEY:VALUE                                        |
| ----------- | ------ | ------------------------------------|-------------------------------------------------------------------------------------------------------- |-------------------------------------------------------- |
| Create user | POST   | http://localhost:8080/app-ws/users  | { "firstName":"Vladas", "lastName":"Uponavicius", "email":"uponavicius@gmail.com", "password":"123444"} | Content-Type:application/json, Accept:application/json  |

- Go to Your email, open letter with subject **One last step to complete your registration with FamFinApp** and click link.

#### Reset password
- Send request for password reset with Postman

| Action         | Method | URL                                                      | Body JSON Example                 | Header KEY:VALUE                                       |
| -------------- | ------ | -------------------------------------------------------- |---------------------------------- |------------------------------------------------------- |
| Password reset | POST   |http://localhost:8080/app-ws/users/password-reset-request | {"email":"uponavicius@gmail.com"} | Content-Type:application/json, Accept:application/json |

- Go to Your email, open letter with subject **Password reset request - FamFinApp** and click link.
- Then on a browser type "New password",  "Retype new password" and click button "Save new password".
- Try to login FamFin-app-ws with a new password, using Postman

| Action  | Method | URL                                       | Body JSON Example                                      | Header KEY:VALUE                                       |
| ------- | ------ | ----------------------------------------- |------------------------------------------------------- |------------------------------------------------------- |
| Login   | GET    | http://localhost:8080/app-ws/users/login  | {"email":"uponavicius@gmail.com", "password":"123444"} | Content-Type:application/json, Accept:application/json |




